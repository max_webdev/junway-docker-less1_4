FROM debian:9 as build
RUN apt update && apt install -y wget gcc make g++ perl libssl-dev
RUN wget https://ftp.pcre.org/pub/pcre/pcre-8.44.tar.gz \
    && tar xvfz pcre-8.44.tar.gz \
    && cd pcre-8.44 \
    && ./configure \
    && make \
    && make install
RUN wget http://zlib.net/zlib-1.2.11.tar.gz \
    && tar xvfz zlib-1.2.11.tar.gz \
    && cd zlib-1.2.11 \
    && ./configure \
    && make \
    && make install
RUN wget http://nginx.org/download/nginx-1.19.3.tar.gz \
	&& tar xvfz nginx-1.19.3.tar.gz \
	&& cd nginx-1.19.3 \
	&& ./configure \
        --with-http_ssl_module \
        --with-stream \
        --with-pcre=../pcre-8.44 \
        --with-zlib=../zlib-1.2.11 \
    && make \
    && make install
	

FROM debian:9
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/sbin/nginx .
COPY --from=build /usr/lib/x86_64-linux-gnu/libssl.so.1.1 /usr/lib/x86_64-linux-gnu/
COPY --from=build /usr/lib/x86_64-linux-gnu/libcrypto.so.1.1 /usr/lib/x86_64-linux-gnu/
RUN mkdir ../logs ../conf \
	&& touch ../logs/error.log \
	&& chmod +x nginx
CMD ["./nginx", "-g", "daemon off;"]